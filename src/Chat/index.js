import Dialog from '../Dialog';
import Header from '../Header';
import Footer from '../Footer';

import './styles.css';

class Chat {
  constructor(node, state, sendMessage) {
    this.node = node;
    this.state = state;
    this.wrapper = null;
    this.sendMessage = sendMessage;
  }

  renderHeader() {
    new Header(this.wrapper).render();
  }

  renderContent() {
    new Dialog(this.wrapper, this.state).render();
  }

  renderFooter() {
    new Footer(this.wrapper, this.sendMessage).render();
  }

  render() {
    this.wrapper = document.getElementsByClassName('chat')[0];

    if (!this.wrapper) {
      this.wrapper = document.createElement('div');
      this.wrapper.classList.add('chat');
      this.node.appendChild(this.wrapper);
    }

    this.renderHeader();
    this.renderContent();
    this.renderFooter();
  }
}

export default Chat;
