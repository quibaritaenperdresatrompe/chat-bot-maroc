import './styles.css';

class Dialog {
  constructor(node, state) {
    this.node = node;
    this.list = null;
    this.messages = state.messages;
  }

  renderMessage(message) {
    const messageNode = document.createElement('li');
    messageNode.classList.add('chat-message');
    if (message.isSent) {
      messageNode.classList.add('chat-message-user');
    }

    const messageContent = document.createTextNode(message.text);
    messageNode.appendChild(messageContent);

    this.list.appendChild(messageNode);
  }

  renderMessages() {
    const messageNodes = document.getElementsByClassName('chat-message');
    const messageNodesArray = [...messageNodes];
    messageNodesArray.forEach(messageNode => messageNode.remove());

    this.messages.forEach(message => {
      this.renderMessage(message);
    });
  }

  render() {
    this.list = document.getElementsByClassName('chat-messages')[0];
    if (!this.list) {
      this.list = document.createElement('ol');
      this.list.classList.add('chat-messages');
      this.node.appendChild(this.list);
    }

    this.renderMessages();
  }
}

export default Dialog;
