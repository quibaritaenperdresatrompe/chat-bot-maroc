import './styles.css';

class Header {
  constructor(node) {
    this.node = node;
  }

  render() {
    let wrapper = document.getElementsByClassName('chat-header')[0];
    if (!wrapper) {
      wrapper = document.createElement('header');
      wrapper.classList.add('chat-header');

      const headerContent = document.createTextNode('Chat Bot');
      wrapper.appendChild(headerContent);

      this.node.appendChild(wrapper);
    }
  }
}

export default Header;
