import Chat from '../Chat';

class App {
  constructor(appNode) {
    this.appNode = appNode;
    this.state = {
      messages: [{ text: 'Salam !', isSent: false }, { text: 'Labas 3lik ?', isSent: false }],
    };
    this.chat = null;
  }

  sendMessage(messageText) {
    this.state.messages.push({
      text: messageText,
      isSent: true,
    });
    this.render();
  }

  render() {
    this.chat = new Chat(this.appNode, this.state, this.sendMessage.bind(this));
    this.chat.render();
  }
}

export default App;
