import './styles.css';

class Footer {
  constructor(node, sendMessage) {
    this.node = node;
    this.footer = null;
    this.sendMessage = sendMessage;
    this.input = null;
  }

  handleButtonClick() {
    if (this.input.value.trim() !== '') {
      this.sendMessage(this.input.value);
    }
    this.input.value = '';
  }

  renderButton() {
    const button = document.createElement('button');
    button.classList.add('chat-button');

    const buttonText = document.createTextNode('OK');
    button.appendChild(buttonText);

    button.addEventListener('click', this.handleButtonClick.bind(this));

    this.footer.appendChild(button);
  }

  renderInput() {
    this.input = document.createElement('input');
    this.input.classList.add('chat-input');

    this.input.setAttribute('placeholder', 'Entrez un message...');

    this.footer.appendChild(this.input);
  }

  render() {
    this.footer = document.getElementsByClassName('chat-footer')[0];
    if (!this.footer) {
      this.footer = document.createElement('div');
      this.footer.classList.add('chat-footer');

      this.node.appendChild(this.footer);

      this.renderInput();
      this.renderButton();
    }
  }
}

export default Footer;
