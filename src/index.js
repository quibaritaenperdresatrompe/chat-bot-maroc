import App from './App';

(() => {
  const appNode = document.getElementById('app');
  new App(appNode).render();
})();
